import Ember from 'ember';
import ENV from 'movie-search/config/environment';

export default Ember.Route.extend({
  queryParams: {
    page: {
      refreshModel: true
    }
  },
  model: function (params) {
    let key = ENV.APP.themoviedbApiKey;
    let uri = ENV.APP.themoviedbApiURL;

    let that = this;

    if (!Ember.isPresent(params.page)) {
      params.page = 1;
    }

    uri += 'search/movie?api_key=' + key + '&query=' + params.query + '&page=' + params.page;

    return Ember.$.getJSON(uri).then(function (data) {
      let models = {
          items: [],
          pagesCount: data.total_pages,
          currentPage: data.page,
          pages: (function () {
            let pages = [];
            for (let i = 1; i < data.total_pages; i++) {
              pages.push(i);
            }
            return pages;
          })(),
        };
      models.needPaginate = models.pagesCount > 1;

      Ember.$.each(data.results, function (index, el) {
        models.items.push(that.store.findRecord('movie', el.id));
      });

      return models;
    });

  }
});
