import DS from 'ember-data';

export default DS.Transform.extend({
  deserialize(serialized) {
    serialized = moment(serialized, "YYYY-MM-DD").format('DD.MM.YYYY');
    return serialized;
  },

  serialize(deserialized) {
    return deserialized;
  }
});
