import Ember from 'ember';

export function dec(params) {
  return params[0] - 1;
}

export default Ember.Helper.helper(dec);
