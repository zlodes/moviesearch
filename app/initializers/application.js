export function initialize(application) {
  // Router Service
  application.inject('component', 'router', 'router:main');
  application.inject('controller', 'router', 'router:main');
  
}

export default {
  name: 'application',
  initialize
};
