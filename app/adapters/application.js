import DS from 'ember-data';

export default DS.RESTAdapter.extend({
  host: 'https://api.tmdb.org',
  headers: {
    'language': 'ru'
  },
  namespace: '3',
  pathForType: function (type) {
    return Ember.String.underscore(type);
  }
});
