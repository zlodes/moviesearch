import ApplicationAdapter from './application';
import ENV from 'movie-search/config/environment';

export default ApplicationAdapter.extend({
  findRecord: function (store, type, id, snapshot) {
    let url = ENV.APP.themoviedbApiURL + [type.modelName, id].join('/') +
      '?language=ru&api_key=' +
      ENV.APP.themoviedbApiKey;

    return Ember.$.getJSON(url).then(function (data) {
      return data;
    });
  }
});
