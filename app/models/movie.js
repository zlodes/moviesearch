import DS from 'ember-data';
//import ENV from 'movie-search/config/environment';

export default DS.Model.extend({
  title: DS.attr('string'),
  countries: DS.attr(),
  genres: DS.attr(),
  posterPath: DS.attr('string'),
  posters: Ember.computed('posterPath', {
    get: function () {
      let path = this.get('posterPath');

      return {
        w185: 'https://image.tmdb.org/t/p/w185' + path,
        w500: 'https://image.tmdb.org/t/p/w500' + path
      };
    }
  }),
  releaseDate: DS.attr('normal-date'),
  releaseYear: Ember.computed('releaseDate', function () {
    return this.get('releaseDate').replace(/.*?(\d{4})/g, '$1');
  }),
  originalTitle: DS.attr('string'),
  originalLanguage: DS.attr('string'),
  overview: DS.attr('string')

});
