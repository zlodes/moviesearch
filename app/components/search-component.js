import Ember from 'ember';
import ENV from 'movie-search/config/environment';

export default Ember.Component.extend({
  change: function () {
    this.sendAction('action', this.get('value'));
  }
});
