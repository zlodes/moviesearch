import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  serialize: function (snapshot, options) {
    let json = this._super(snapshot, options);

    return json;
  },
  normalizeResponse: function (store, primaryModelClass, payload, id, requestType) {

    let normalized = {
      'movie': {
        id: payload.id,
        title: payload.title,
        originalTitle: payload.original_title,
        countries: payload.production_countries,
        genres: payload.genres,
        posterPath: payload.poster_path,
        releaseDate: payload.release_date,
        originalLanguage: payload.original_language,
        overview: payload.overview
      }
    };

    return this._super(store, primaryModelClass, normalized, id, requestType);
  }
});
