import Ember from 'ember';

export default Ember.Controller.extend({

  history: [],

  hasHistory: Ember.computed('history.length', function () {
    return this.get('history.length') > 1;
  }),

  watchHistory: Ember.observer('router.currentPath', function () {
    let history = this.get('history');
    let currentPath = this.get('router.currentPath');

    if (history[history.length - 2] === currentPath) {
      this.get('history').popObject();
      this.get('history').pushObject(currentPath);
      this.get('history').popObject();
    } else {
      this.get('history').pushObject(currentPath);
    }

  }),

  actions: {
    goBack: function () {
      this.get('history').popObject();
      window.history.back();
      this.get('history').popObject();
    }
  }
});
