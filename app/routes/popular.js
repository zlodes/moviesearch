import Ember from 'ember';
import ENV from 'movie-search/config/environment';

export default Ember.Route.extend({

  model: function () {
    let key = ENV.APP.themoviedbApiKey;
    let uri = ENV.APP.themoviedbApiURL;
    uri += 'movie/popular?language=ru&api_key=' + key;
    let that = this;

    return Ember.$.getJSON(uri).then(function (data) {
      let models = {
        items: [],
        pagesCount: data.total_pages,
        currentPage: data.page,
        pages: (function () {
          let pages = [];
          for (let i = 1; i < data.total_pages; i++) {
            pages.push(i);
          }
          return pages;
        })(),
      };
      models.needPaginate = false;

      Ember.$.each(data.results, function (index, el) {
        models.items.push(that.store.findRecord('movie', el.id));
      });

      return models;
    });
  }
});
