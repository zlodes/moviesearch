import Ember from 'ember';

export function ifCond(params) {
  let v1 = params[0],
    v2 = params[2],
    operator = params[1];

  switch (operator) {
    case '==':
      return v1 == v2;
    case '===':
      return (v1 === v2);
    case '<':
      return (v1 < v2);
    case '<=':
      return (v1 <= v2);
    case '>':
      return (v1 > v2);
    case '>=':
      return (v1 >= v2);
    case '&&':
      return (v1 && v2);
    case '||':
      return (v1 || v2);
    default:
      return false;
  }
}

export default Ember.Helper.helper(ifCond);
